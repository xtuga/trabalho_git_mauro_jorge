package ficha9ex1;
import java.util.*;
public class ficha9ex1
{
	/*Apresentacao do jogo*/
	

	public static void Preencher(String palavra[])
	{
		System.out.println("\n");
		for(int i=0; i<palavra.length; i++)
		{
			if(palavra[i] == null)
				System.out.print("_ ");
			else
				System.out.print(palavra[i] + " ");
		}
	}
	
	//Mostra as letras Erradas
	public static void MostrarErradas(String[] usadas)
	{
		System.out.println("\n\n");
		for(int i=0; i<usadas.length; i++)
			if(usadas[i] != null)
				System.out.print(usadas[i] + " ");
		if(usadas[0] != null)
			System.out.println(" - Letras Usadas");
	}
	
	//Verificar letras que foram usadas
	public static boolean VerificarUsadas(String l,String[] usadas)
	{
		if(!Arrays.asList(usadas).contains(l))
			return true;
		System.out.println("\nEscolha outra letra que n�o esteja usada!\n");
		return false;
	}
	
	//Verificar se e letra valida
	public static boolean VerificarLetra(String l)
	{
		if( Character.isLetter(l.charAt(0)))
			return true;
		System.out.println("\nEscolha uma letra valida!\n");
		return false;
	}
	
	//Verificar se utilizador ja acertou
	public static boolean VerificarPalavra(String[] palavra,String escolhida)
	{
		int count = 0;
		String[] escolhida_nova = escolhida.split("");
		
		for(int i = 0; i < palavra.length; i++)
			if ( palavra[i] != null && palavra[i].equals(escolhida_nova[i]))
				count ++;
		
		if( count == palavra.length)
			return true;
		else
			return false;
	}
	
	//Escolher aleatoriamente uma palavra entre varias categorias
	public static String EscolherPalavra()
	{
		Random rnd = new Random();
		String[] animais = {"CAO","GATO","PASSARO","PEIXE","TIGRE","LEAO","GIRAFA","HIPOPOTAMO","MACACO","ZEBRA" };//#1 Categoria
		String[] paises = {"FRANCA","PORTUGAL","ALEMANHA","ESPANHA","ANGOLA","INGLATERRA","BRASIL","ITALIA","GRECIA","JAPAO" };//#2 Categoria
		String[] marcas_carro = {"FIAT", "LAMBORGHINI", "LOTUS", "FORD", "CHEVROLET", "RENAULT", "SEAT", "VOLKSWAGEN","AUDI", "MERCEDES"};//#3 Categoria
		String[] jogos = {"FALLOUT","FIFA","COUNTER STRIKE","CALL OF DUTY","BATTLEFIELD","MINECRAFT","NEED FOR SPEED","GRAND THEFT AUTO", "POKEMON","SUPER MARIO"};//#4 Categoria
		String[] filmes = {"MINIONS","FAST AND FURIOUS","HANGOVER","DUMB AND DUMBER","STAR WARS","LORD OF THE RINGS","THE HOBBIT","AVENGERS","IRON MAN","THOR"};//#5 Filmes
		
		
		int cat = rnd.nextInt(5),palavra = rnd.nextInt(10);
		switch(cat)
		{
			case 0: System.out.println("Categoria: ANIMAIS\n");
					return animais[palavra];
			case 1: System.out.println("Categoria: PAISES\n");
					return paises[palavra];
			case 2: System.out.println("Categoria: MARCAS DE CARRO\n");
					return marcas_carro[palavra];
			case 3: System.out.println("Categoria: JOGOS\n");
					return jogos[palavra];
			case 4: System.out.println("Categoria: FILMES\n");
					return filmes[palavra];
			default:return "";
		}
	}
	
	
	public static void main(String[] args)
	{
		System.out.println("JOGO DA FORCA\n");//Mensagem de inicio
		
		String escolhida = EscolherPalavra();//palavra escolhida
		int t = escolhida.length();//Ser� necess�rio o tamanho da palavra escolhida
		
		String[] palavra = new String[t];
		String[] usadas = new String[26];
		String l = "";
		
		Scanner scan;
		int tentativas = t;
		
		if(tentativas > 20)
			tentativas = 20;
		
		for(int i = 0; i < palavra.length; i++)
			if( Character.isWhitespace(escolhida.charAt(i)) )
				palavra[i] = " ";
		
			
		for(int i = 0; i < tentativas && !VerificarPalavra(palavra,escolhida); i++)
		{
			do
			{
				Preencher(palavra);
				MostrarErradas(usadas);
				System.out.print("\n\nN� Tentativas: " + ((tentativas)-i));
				System.out.print("\nEscolha uma letra: ");
				
				scan = new Scanner(System.in);
				l = scan.nextLine().toUpperCase().substring(0, 1);
				
			}while(!VerificarUsadas(l,usadas) || !VerificarLetra(l));
			
			for (int j = escolhida.indexOf(l); j >= 0; j = escolhida.indexOf(l, j + 1))
				palavra[j] = l;
			
			usadas[i] = l;
		}
		
		if ( !VerificarPalavra(palavra,escolhida) )
			System.out.println("Jogo da forca terminado! N�o conseguiu a tempo! A palavra era: " + escolhida);
		else
			System.out.println("\nJogo da forca terminado!\nConseguiu adivinhar a palavra: " + escolhida);
	}

}
