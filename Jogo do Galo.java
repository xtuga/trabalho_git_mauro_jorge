package ficha5ex4;
public class ficha5ex4
{
	/*Desenhar o Jogo*/
	public static void DesenharJogo(char[] marcas)
	{
		String jogo = "\n\n";
		for(int i=1; i<=9; i++)
		{
			if(marcas[i - 1] == 0)
				jogo += i;
			else
				jogo += marcas[i - 1];
			
			if(i % 3 == 0)
				jogo += "\n\n";
			else
				jogo += "\t";
		}
		StdOut.print(jogo);
	}
	/*Verificar Posi�ao*/
	public static boolean VerPos(int[] args, int pos)
	{
		for(int i=0; i<args.length; i++)
			if(args[i] == pos)
				return false;
		return true;
	}
	
	/*Fun�ao pa jogar*/
	public static int Jogar(char[] marcas, int[] posicoes,String nome)
	{
		DesenharJogo(marcas);
		int p;
		do
		{
			StdOut.print("\n\n" + nome +", escolha uma posicao(1-9): ");
			p = StdIn.readInt();
			if(!(p>= 1 && p<= 9) || !VerPos(posicoes,p))
				StdOut.print("Posicao Invalida!");
		}while(!(p>= 1 && p<= 9) || !VerPos(posicoes,p));
		return p;
	}
	/*Verificar estado do jogo*/
	public static int VerJogo(char[] marcas)
	{
		int j = 0;
		for(int i = 0; i < 3; i++)
		{
			if(marcas[j] == 'O' && marcas[j+1] == 'O' && marcas[j+2] == 'O')// Verificar posicoes horizontais
				return 1;
			if(marcas[j] == 'X' && marcas[j+1] == 'X' && marcas[j+2] == 'X')// Verificar posicoes horizontais
				return 2;
			if(marcas[i] == 'O' && marcas[i+3] == 'O' && marcas[i+6] == 'O')// Verificar posicoes verticais
				return 1;
			if(marcas[i] == 'X' && marcas[i+3] == 'X' && marcas[i+6] == 'X')// Verificar posicoes verticais
				return 2;
			j+=3;
		}
		if((marcas[0] == 'O' && marcas[4] == 'O' && marcas[8] == 'O') || (marcas[2] == 'O' && marcas[4] == 'O' && marcas[6] == 'O'))
			return 1;
		if((marcas[0] == 'X' && marcas[4] == 'X' && marcas[8] == 'X') || (marcas[2] == 'X' && marcas[4] == 'X' && marcas[6] == 'X'))
			return 2;
		
		return 0;
	}
	
	
	public static void main(String[] args)
	{
		int p, j = 1, v = 0;
		int[] posicoes = new int[9];//posicoes inseridas
		char[] marcas = new char[9];//marca de cada posicao
		String[] nomes = new String[2];
		
		for(int i = 0; i < 2; i++)
		{
			StdOut.print("Nome do " + (i+1) +". Jogador : ");
			nomes[i] = StdIn.readLine();
		}
		
		for(int i = 0; i < 5 && v == 0; i++)
		{
			p = Jogar(marcas, posicoes, nomes[0]);//
			posicoes[i] = p;//guardar a posicao inserida
			marcas[p-1] = 'O';//primeiro jogador � sempre com a marca O, e vamos colocar na posicao que escolheu no array
			
			if ( (v = VerJogo(marcas)) != 0)
				break;
			
			if(i != 4)
			{
				p = Jogar(marcas, posicoes, nomes[1]);
				posicoes[j] = p;
				marcas[posicoes[j] - 1] = 'X';
				j += 2;
			}
			v = VerJogo(marcas);
		}
		DesenharJogo(marcas);
		System.out.println("\n\nJOGO TERMINADO!");
		if(v != 0)
			System.out.println("Ganhou: " + nomes[v-1]);
		else
			System.out.println("Ninguem Ganhou!");
	}
}
