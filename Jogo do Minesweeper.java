package ficha6ex1;
import java.util.*;

public class ficha6ex1
{

	//Apresenta a posicao dada
	public static void apresentarpos(int[][] posicoes, int pos)
	{	
		String apr = "";
		
		if( posicoes[pos][0] < 10)
			apr += "    ";
		else if( posicoes[pos][0] < 100)
			apr += "   " ;
		else
			apr += "  ";
		
		if( posicoes[pos][1] == 0)//se a posicao estiver vazia (sem bomba ou ja selecionada)
		{
			if( posicoes[pos][0] < 10)
				apr += "- ";
			else
				apr += "-- ";
		}
		else
			apr += posicoes[pos][0] + " ";
		
		StdOut.print(apr);
	}
	
	//apresentacao grafica do jogo
	public static void desenharjogo(int[][] posicoes, int quant)
	{
		int mudarL = posicoes.length / quant;

		for(int i = 0; i < posicoes.length; i++)
		{
			if( i % mudarL == 0 )
				StdOut.print("\n");
			
			apresentarpos(posicoes, i);
		}
	}
	
	//Adicionar Bombas!
	public static int[][] addbombas(int[][] posicoes, int diff)
	{
		Random rnd = new Random();
		int quant = posicoes.length/3,//numero de bombas (Ex: 9/3 = 3 bombas em F�cil)
			count = 0,
			random = 0;
		
		for(int i = 0; i < posicoes.length; i++)
		{
			random = rnd.nextInt( 100 ) + 1;//um numero entre 1-100
			
			if( random >= 50 && count < quant)
			{
				posicoes[i][1] = 1;
				count++;
			}
			else
				posicoes[i][1] = 2;
			
			posicoes[i][0] = i;
			StdOut.print(posicoes[i][0] + " | " + posicoes[i][1] + "\n");
		}
		return posicoes;
	}
	
	//Verificar se encontrou bomba
	public static boolean encontrarbomba(int[][] posicoes, int pos)
	{
		if(posicoes[pos][1] == 1)
			return true;//encontrou bomba
		return false;
	}
	
	//Verificar se encontrou uma posicao sem bomba
	public static boolean verificarvitoria(int[][] posicoes)
	{
		for(int i = 0; i<posicoes.length; i++)
			if(posicoes[i][1] == 2)
				return true;//encontrou uma posicao ainda nao selecionada
		return false;
	}
	
	public static void main(String[] args)
	{
		int diff = 0, pos = 0;
		do
		{
			StdOut.print("Escolha a dificuldade do jogo -\n\n1)Facil\n2)Normal\n3)Dificil\n\nResposta: ");
			diff = StdIn.readInt();
			if(!(diff >= 1 && diff <= 3))
				StdOut.print("Opcao Invalida");
		}while(!(diff >= 1 && diff <= 3));//decidir dificuldade
		
		int quant = 3*diff,//numero de posicoes na primeira fila
			total = quant*quant;//total de posicoes
		
		int posicoes[][] = new int[total][2];//primeira casa para as posicoes, segunda para verificar se tem bomba ou nao

		posicoes = addbombas(posicoes, diff);//adicionar bombas em algumas posicoes
		
		do
		{
			desenharjogo(posicoes, quant);//apresentar o jogo
			
			StdOut.print("\n\nPosicao: ");
			pos = StdIn.readInt();
			
			if(!(pos >= 0 && pos < total))//verificar se esta dentro do range das posicoes
			{
				StdOut.print("Posicao Invalida\n");
				pos = 0;
			}
			else if(encontrarbomba(posicoes, pos))
				StdOut.print("BOMBAAAA!!! PERDEU!!!");
			else	
				posicoes[pos][1] = 0;//se a posicao for v�lida, torna-se uma posicao selecionada
			
			if(!verificarvitoria(posicoes))
				StdOut.print("GANHOU!!!");
			
		}while(!encontrarbomba(posicoes, pos) && verificarvitoria(posicoes));
	}

}
